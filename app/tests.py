from cookbook_app import app
from fastapi.testclient import TestClient

client = TestClient(app)


def test_list_of_recipes():
    response = client.get("/recipes")
    assert response.status_code == 200


def test_detail_recipe():
    response = client.get("/recipe/1")
    assert response.status_code == 200
