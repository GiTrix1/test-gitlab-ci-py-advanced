from pydantic import BaseModel


class CookBook(BaseModel):
    title: str
    number_of_views: int
    cooking_time_in_minutes: int


class CookBookIn(CookBook):
    ...


class CookBookOut(CookBook):
    id: int

    class Config:
        orm_mode = True


class DetailRecipe(BaseModel):
    recipe_id: int
    title: str
    quantity: int
    unit: str


class DetailRecipeIn(DetailRecipe):
    ...


class DetailRecipeOut(DetailRecipe):
    id: int

    class Config:
        orm_mode = True
