from typing import Any, List, Sequence, Union

import cookbook_db_models
import cookbook_schemas
from cookbook_db import engine, session
from fastapi import FastAPI, Path
from sqlalchemy import Row, RowMapping
from sqlalchemy.future import select

app = FastAPI(title="cookbook")


@app.on_event("startup")
async def shutdown():
    async with engine.begin() as conn:
        await conn.run_sync(cookbook_db_models.Base.metadata.create_all)


@app.on_event("shutdown")  # type: ignore
async def shutdown():
    await session.close()
    await session.dispose()


@app.get("/recipes", response_model=List[cookbook_schemas.CookBookOut])
async def list_of_recipes() -> Sequence[Union[Union[Row, RowMapping], Any]]:
    list_recipes = await session.execute(
        select(cookbook_db_models.CookBook)
        .order_by(cookbook_db_models.CookBook.number_of_views.desc())
        .order_by(cookbook_db_models.CookBook.cooking_time_in_minutes)
    )
    return list_recipes.scalars().all()


@app.get("/recipe/{id_recipe}", response_model=List[cookbook_schemas.DetailRecipeOut])
async def detail_recipe(
    id_recipe: int = Path(
        ...,
        title="Id recipe on dinner",
        ge=1,
        le=3,
    )
) -> Sequence[Union[Union[Row, RowMapping], Any]]:
    result_recipe = await session.execute(
        select(cookbook_db_models.DetailRecipe).filter(
            cookbook_db_models.DetailRecipe.recipe_id == id_recipe
        )
    )
    return result_recipe.scalars().all()
