from cookbook_db import Base
from sqlalchemy import Column, ForeignKey, Integer, String


class CookBook(Base):
    __tablename__ = "CookBook"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    number_of_views = Column(Integer, index=True)
    cooking_time_in_minutes = Column(Integer, index=True)


class DetailRecipe(Base):
    __tablename__ = "DetailRecipe"
    id = Column(Integer, primary_key=True, index=True)
    recipe_id = Column(Integer, ForeignKey("CookBook.id"), nullable=False)
    title = Column(String, index=True)
    quantity = Column(Integer, index=True)
    unit = Column(String, index=True)
